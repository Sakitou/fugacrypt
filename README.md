# Fugacrypt

Fugacrypt is a simple Python script for encrypting and decrypting files using AES encryption.

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/Sakitou/fugacrypt.git
   ```

2. Navigate to the project directory:

   ```bash
   cd fugacrypt
   ```

3. Run the installation script:

   ```bash
   bash install.sh
   ```

## Usage

### Encrypt a file:

```bash
fugacrypt encrypt --filename input_file.txt --password your_password --outfile encrypted_file.crypto
```

### Decrypt a file:

```bash
fugacrypt decrypt --filename encrypted_file.crypto --password your_password --outfile decrypted_file.txt
```

## Dependencies

- cryptography
- bcrypt

## Download

[Google Drive](https://drive.google.com/drive/folders/1fMVHybRIIDaySOQKv4j1EXmpBtblor4d?usp=sharing)
